import { Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'WebApplication';
  counter: number[] = [10, 23, 44, 57, 1, 2]
  mainText!:string;
  secondText!:string;
}
